package com.example.demo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


import java.util.List;

public interface EmployeeRepository extends CrudRepository<Employee, Integer>{
	
	public List<Employee> findByName(String name);
	@Query("Select e from Employee e where salary >= :salary")
	public List<Employee> listNameBySalaryOver(@Param("salary") float salary);
	
	

}
