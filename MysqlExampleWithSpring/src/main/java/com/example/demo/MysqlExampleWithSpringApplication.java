package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MysqlExampleWithSpringApplication implements CommandLineRunner{

	@Autowired
	EmployeeRepository repo;
	public static void main(String[] args) {
		SpringApplication.run(MysqlExampleWithSpringApplication.class, args);
	}
	@Override
	public void run(String... args) throws Exception {
		repo.save(new Employee("Amit", 32000));
		repo.save(new Employee("Sumit", 41090));
		repo.save(new Employee("Lavanya", 21000));
		repo.save(new Employee("Shikha", 43598));
		repo.save(new Employee("Naveen", 40876));
		repo.save(new Employee("Sahil", 18000));
		repo.save(new Employee("Siddharth", 30000));
		
		
		Iterable<Employee> ite= repo.findAll();
		
		System.out.println("All the employee");
		ite.forEach(name-> System.out.println(name));
		
		List<Employee> amit= repo.findByName("amit");
		System.out.println("Details of Amit?");
		amit.forEach(name-> System.out.println(name));
		
		List<Employee> maxSal=repo.listNameBySalaryOver(40000);
		System.out.println("Employee with salary over 40K.");
		maxSal.forEach(name->System.out.println(name));
		
	}

}
