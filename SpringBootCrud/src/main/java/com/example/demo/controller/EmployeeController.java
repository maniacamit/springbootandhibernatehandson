package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.EmployeeDAO;
import com.example.demo.model.Employee;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeDAO empdao;
	
	@PostMapping("/saveEmp")
	public String saveEmployee(@RequestBody List<Employee> employee) {
		empdao.saveAll(employee);
		return "done";
	}
	
	@GetMapping("/getEmp")
	public List<Employee> getEmp(){
		return (List<Employee>) empdao.findAll();
	}
	
	@DeleteMapping("/deleteEmp/{emp_id}")
	public String deleteEmp(@PathVariable("emp_id") int emp_id,Employee employee) {
		employee.setEmp_id(emp_id);
		empdao.deleteById(employee.getEmp_id());
		return "done deleteing";
	}
}
